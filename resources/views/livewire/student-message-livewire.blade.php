
<head>

    <!-- Tailwind CSS -->
    <link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet">


  </head>
<x-slot name="header">
    <h1 class="text-center">Message</h1>
</x-slot>
<div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg px-4 py-4">
            <div class="p-10 grid grid-cols-1 sm:grid-cols-1 md:grid-cols-3 lg:grid-cols-3 xl:grid-cols-3 gap-5">


            @foreach ($message as $msg)
            @foreach ($sections as $section)
            @if ($msg->section_id == $section->pivot->section_id)
            @if ($msg->typeOf=="general" && $msg->active !=0 )
            <div class="rounded overflow-hidden shadow-lg bg-blue-300">
              <div class="px-6 py-4 ">
                <div class="font-bold text-xl mb-2 capitalize">{{$msg->title}}</div>
                <p class=" text-xl text-base">
                    {{$msg->message}}
                </p>
              </div>
              <div class="px-6 pt-4 pb-2">
                <span class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">Course:{{$msg->section->course->name}}</span>
                <span class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">Teacher:{{$msg->section->teacher->name}}</span>
              </div>
            </div>
            @elseif ($msg->typeOf=="Scu" && $msg->active !=0 )
             <?php $total=$section->pivot->first + $section->pivot->mid+$section->pivot->final;?>
             @if ($total>=85)
             <div class="rounded overflow-hidden shadow-lg bg-blue-300">
                <div class="px-6 py-4 ">
                  <div class="font-bold text-xl mb-2 capitalize">{{$msg->title}}</div>
                  <p class=" text-xl text-base">
                      {{$msg->message}}
                  </p>
                </div>
                <div class="px-6 pt-4 pb-2">
                  <span class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">Course:{{$msg->section->course->name}}</span>
                  <span class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">Teacher:{{$msg->section->teacher->name}}</span>
                </div>
              </div>
             @endif
            @endif
            @endif
                @endforeach
                @endforeach
              </div>
            </div>
            </div>
</div>





