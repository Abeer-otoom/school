<?php
use App\Http\Livewire\TeacherdashLivewire;
?>

<x-slot name="header">
    <h1 class="text-center">Message</h1>
</x-slot>
<div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg px-4 py-4">
            @if (session()->has('message'))
            <div class="bg-green-600 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md my-3"
                role="alert">
                <div class="flex">
                    <div>
                        <b class="text-sm text-center text-green-500">{{ session('message') }}</b>
                    </div>
                </div>
            </div>
            @endif
<table class="table-fixed w-full">
    <thead>
        @if (!$messages->isEmpty())
        <tr class="bg-gray-100">
            <th class="px-4 py-2 w-8 ">Title</th>
            <th class="px-4 py-2 w-14 ">Message</th>
            <th class="px-4 py-2 w-14 ">First Date</th>
            <th class="px-4 py-2 w-14 ">Last Date</th>
            <th class="px-4 py-2 w-8 ">Type</th>
            <th class="px-4 py-2 w-8 ">Delete Message</th>
        </tr>


    </thead>
    <tbody>

        @foreach ($messages as $msg)

        @foreach ($sections as $section)

        @if ($msg->section_id == $section->id)
        <tr>

            <td class="border px-4 py-2 ">{{$msg->title}}</td>
            <td class="border px-4 py-2 ">{{$msg->message}}</td>
            <td class="border px-4 py-2 ">{{$msg->first_date}}</td>
            @if ($currentDate > $msg->last_date)
            <?php TeacherdashLivewire::updateActive($msg->id);?>
            <td class="border px-4 py-2 text-red-600 ">
            <button wire:click="confirmActivate({{$msg->id}})" type="button"><b>{{$msg->last_date}}</b> *Expiry Date</button></td>
            @if($isActivateOpen)
            @include('livewire.teacher-dashboard.activate-message')
            @endif
            @else
            <td class="border px-4 py-2  ">{{$msg->last_date}}</td>
            @endif

            <td class="border px-4 py-2 ">{{$msg->typeOf}} </td>
            <td class="border px-4 py-2">
                <button wire:click="confirm({{ $msg->id }})" class="px-3 py-2 text-white bg-red-600" type="button"
                 >Delete</button>
                 @if($isDeleteOpen)
                 @include('livewire.teacher-dashboard.delete-message')
                 @endif
            </td>
        </tr>

        @endif
        @endforeach

        @endforeach
    </tbody>
</table>
@else
<p class="text-center text-red-600 text-xl px-6 py-2"><b>No Message</b></p>
@endif

{{-- <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script type="text/javascript">
    tinymce.init({
      selector: '#mytextarea'
    });
    </script> --}}


