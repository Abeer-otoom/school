<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Message;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Teacher;
use App\Models\Section;

class MessageLivewire extends Component
{
    public $lastDate , $messages , $active;
    public $title , $message , $messageId;
    public $messageType;
    public $teacher , $teacherId , $sections , $currentDate;
    public $isDeleteOpen=false;
    public $isActivateOpen=false;
    public $isUpdateOpen=false;
    /**
     * return teacherId,all section that belong to this teacher
     * and all message.
     * @author Abeer Otoom <abeer2otoom@gmail.com>
     * @return view
     */
    public function render()
    {   $this->currentDate=date('Y-m-d');
        $email=Auth::user()->email;
        $user=User::all()->where('email',$email)->first();
        $this->teacher=Teacher::all()->where('user_id',$user->id)->first();
        $this->teacherId=$this->teacher->id;
        $this->teacher=Teacher::find($this->teacherId);
        $this->sections=$this->teacher->sections->all();
        $this->messages = Message::all();
        return view('livewire.teacher-dashboard.message-livewire');
    }
    /**
     * close all modal.
     * @author Abeer Otoom <abeer2otoom@gmail.com>
     */
    public function closeModal()
    {
        $this->isDeleteOpen = false;
        $this->isActivateOpen=false;
        $this->isUpdateOpen=false;
    }
    /**
     * open delete message Modal.
     * @author Abeer Otoom <abeer2otoom@gmail.com>
     */
    public function openDeletePopover()
    {
        $this->isDeleteOpen = true;
    }
    /**
     * open Activate message Modal.
     * @author Abeer Otoom <abeer2otoom@gmail.com>
     */
    public function openActivatePopover()
    {
        $this->isActivateOpen = true;
    }
     /**
     * open update message Modal.
     * @author Abeer Otoom <abeer2otoom@gmail.com>
     */
    public function openUpdatePopover()
    {
        $this->isUpdateOpen = true;
    }
    /**
     *open modal for Activate confirmation message.
     * @author Abeer Otoom <abeer2otoom@gmail.com>
     * @param  messageId
     */
    public function confirmActivate($id){
        $this->messageId=$id;
        $this->openActivatePopover();
    }
     /**
     *open modal for delete confirmation message.
     * @author Abeer Otoom <abeer2otoom@gmail.com>
     * @param  messageId
     */
    public function confirm($id){
        $this->messageId=$id;
        $this->openDeletePopover();
    }
     /**
     *delete specific message .
     * @author Abeer Otoom <abeer2otoom@gmail.com>
     */
    public function deleteMessage(){
        Message::find($this->messageId)->delete();
        $this->closeModal();
    }
      /**
     *update lastDate for  specific message .
     * @author Abeer Otoom <abeer2otoom@gmail.com>
     */
    public function updateDate(){

        $message=Message::find($this->messageId);
        $message->update(['last_date'=>$this->lastDate , 'active'=>"1"]);
        $this->closeModal();
    }
}
