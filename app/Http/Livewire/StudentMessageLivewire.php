<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Message;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Student;
use App\Models\Section;
class StudentMessageLivewire extends Component
{
    public $message , $studentMark , $currentDate , $student , $sectionId , $studentId , $sections;
    /**
     * return studentId,all section that belong to this student
     * and all message.
     * @author Abeer Otoom <abeer2otoom@gmail.com>
     * @return view
     */

    public function render()
    {
        $email=Auth::user()->email;
        $user=User::all()->where('email',$email)->first();
        $this->student=Student::all()->where('user_id',$user->id)->first();
        $this->studentId=$this->student->id;
        $this->currentDate=date('Y-m-d');
        $this->student=Student::find($this->studentId);
        $this->sections=$this->student->sections->all();
        $this->message=Message::all();

        return view('livewire.student-message-livewire');
    }
}
