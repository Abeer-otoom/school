<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use HasFactory;
    protected $fillable = [
        'title',
        'message',
        'typeOf',
        'active',
        'first_date',
        'last_date',
        'section_id',
    ];

    public function section()
    {
        return $this->belongsTo(Section::class);
    }
}
