<?php

use Illuminate\Support\Facades\Route;
use App\Http\Livewire\TeacherLivewire;
use App\Http\Livewire\StudentLivewire;
use App\Http\Livewire\CourseLivewire;
use App\Http\Livewire\StudentCoursesLivewire;
use App\Http\Livewire\StudentTaskLivewire;
use App\Http\Livewire\TeacherdashLivewire;
use App\Http\Livewire\MessageLivewire;
use App\Http\Livewire\StudentMessageLivewire;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
Route::get('teachers', TeacherLivewire::class)->name('teacher');
Route::get('students', StudentLivewire::class)->name('students');
Route::get('courses', CourseLivewire::class)->name('courses');

Route::get('teachercourses', TeacherdashLivewire::class)->name('course');
Route::get('studentCourses', StudentCoursesLivewire::class)->name('studentCourses');

Route::get('studentTask', StudentTaskLivewire::class)->name('studentTask');

Route::get('message',MessageLivewire::class)->name('teachermessage');

Route::get('Message',StudentMessageLivewire::class)->name('message');



